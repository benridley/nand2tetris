// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

// Screen map starts at address 16384 and ends at address 24575 (Inclusive)
(RESETCOUNTER)
    @SCREEN
    D=A
    @CURRENTLOCATION    // Initialise the memory to track the current location on the screen
    M=D
(LOOPKBD)
    @CURRENTLOCATION
    D=M
    @24576              // Load constant that represents the final row in the keyboard map
    D=D-A               // Check if the value of CURRENTLOCATION is pointing past the end of the map
    @RESETCOUNTER
    D;JEQ               // Jump back to top and reset the CURRENTLOCATION.
    @KBD                // Load keyboard map into register
    D=M
    @BLACK
    D;JNE               // blackens the screen if the keyboard map has something in it
    @WHITE
    D;JEQ               // whitens the screen if the keyboard map has nothing in it

(WHITE)
    @CURRENTLOCATION
    A=M
    M=0
    @CURRENTLOCATION
    M=M+1
    @LOOPKBD
    0;JMP

(BLACK)
    @CURRENTLOCATION    // Load current location
    A=M                 // Point address at the value stored in currentlocation
    M=-1                // Set that row to black
    @CURRENTLOCATION
    M=M+1               // Reload currentloaction and increment. 
    @LOOPKBD
    0;JMP