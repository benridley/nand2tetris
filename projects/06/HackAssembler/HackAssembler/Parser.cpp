
#include "Parser.h"


Parser::Parser() : inputStream(nullptr)
{

}

Parser::Parser(std::istream* inputStream) : inputStream(inputStream)
{
	// This constructor will open the stream and read the first command into memory.

}


Parser::~Parser()
{
}

bool Parser::hasMoreCommands()
{
	*inputStream >> std::ws;
	if (inputStream->eof() || inputStream->fail())
	{
		return false;
	}
	return true;
}

void Parser::advance()
{
	// Clear current lexing details.
	symbol = "";
	comp = "";
	dest = "";
	jump = "";

	// Declare a buffer that will hold commands as they are lexed.
	std::stringstream lexBuffer;
	char scanner;
	*inputStream >> std::ws >> scanner;
	
	// A_COMMAND
	if(scanner == '@')
	{
		// Loading a constant into A 
		if(isdigit(inputStream->peek()))
		{
			while(inputStream->peek() != EOF && inputStream->peek() != '\n')
			{
				*inputStream >> scanner;
				if(!isdigit(scanner))
				{
					throw std::logic_error("Invalid symbol. Symbols cannot begin with a number, expected a constant.");
				}
				lexBuffer << scanner;
			}
		}

		// Loading a symbol into A
		else if(isValidSymbolCharacter(inputStream->peek()))
		{			
			do
			{
				scanner = inputStream->get();
				if(!isValidSymbolCharacter(scanner)) throw std::logic_error("Invalid characters in Symbol name. Only $_.: and alphabet are allowed.");
				lexBuffer << scanner;
			} while(inputStream->peek() != EOF && !isspace(inputStream->peek()));
		}

		// Invalid A command.
		else
		{
			throw std::logic_error("Bad A_Command. Only constants or symbols with alphabet characters and $_.: are allowed.");
		}

		// Successfully processed an A_COMMAND
		currentCommandType = HackCommand::A_COMMAND;
		symbol = lexBuffer.str();
		return;
	}

	// C_COMMAND
	// Assume if it's not an A_COMMAND, it's a C_COMMAND.
	// Format is dest = comp; jump
	// Where dest or jump may be empty.
	else
	{
		// Start DEST processing.
		// If A, M, or D is the first character, dest may or may not be null.
		if(strchr("AMD", scanner))
		{
			// Save current value as a possible destination. 'Possible' because we don't know if we're in dest or comp at this point.
			lexBuffer << scanner;

			// Get the next non-space character
			*inputStream >>std::ws;
			scanner=inputStream->get();

			// If the next real character is a A, M, or D, continue processing the dest component.
			// Also check to make sure it's not a duplicate. 
			// Finally, this loop will continue to strip whitespace after it's parsed dest in preparation for next checks.
			while(strchr("AMD", scanner) && !strchr((lexBuffer.str().c_str()), scanner))
			{
				lexBuffer << scanner;
				*inputStream >> std::ws;
				scanner = inputStream->get();
			}

			// If next real character is an equals sign, we have reached the end of the populated dest component.
			// Set appropriate values in the parser, and strip trailing whitespace.
			if(scanner == '=')
			{
				this->dest = lexBuffer.str();
				lexBuffer.str("");
				*inputStream >> std::ws >> scanner;;
			}

			// If not, dest is either invalid or null. Move stream back to first character, clear buffer, and process comp component.
			else 
			{
				lexBuffer.str("");
				inputStream->unget();
			}
		}

		// Start COMP processing.

		// If first character is 0,1,-, or !, dest is null so it will fail the above check and process here immediately.
		// If it's A, M, or D, the above check will determine dest is null (or processed) and point scanner to the first comp character.
		// Either way, scanner is now pointing to the first intended comp character.

		// If 0 or 1, we can finish processing comp here. 
		// Example: M = 1.
		if(strchr("01", scanner))
		{
			comp = scanner;
		}
		
		// If - or !, we grab the next non-whhitespace character and terminate processing the comp component.
		else if(strchr("-!", scanner))
		{
			lexBuffer << scanner;
			*inputStream >>std::ws >> scanner;
			if(strchr("AMD", scanner))
			{
				lexBuffer << scanner;
				comp = lexBuffer.str();
			}
		}

		// If the comp command begins with a register, further processing is required.
		else if(strchr("AMD", scanner))
		{
			char reg = scanner;
			lexBuffer << scanner;

			// Check for end of file/line. If so, comp is a single register and we can return.
			if (inputStream->peek() == EOF || inputStream->peek() == '\n')
			{
				comp = lexBuffer.str();
				return;
			}

			*inputStream >>std::ws >> scanner;
			// Next character should be either a unary operator, or semi colon.
			// If unary operator, we should see either another register to operate on or 1.
			if(strchr("+-&|", scanner))
			{
				lexBuffer << scanner;
				*inputStream >>std::ws >> scanner;
				if(strchr("AM", scanner) && reg == 'D')
				{
					lexBuffer << scanner;
					comp = lexBuffer.str();
				}

				else if (strchr("AM", reg) && scanner == 'D')
				{
					lexBuffer << scanner;
					comp = lexBuffer.str();
				}

				else if (scanner == '1')
				{
					lexBuffer << scanner;
					comp = lexBuffer.str();	
				}

				else
				{
					throw std::logic_error("Bad C_COMMAND: Expected a register.");
				}
			}


		}

		// Regardless of comp, get rid of whitespace and check for jump component.
		lexBuffer.str("");
		*inputStream >> std::ws >> scanner;
		if(scanner == ';')
		{
			*inputStream >>std::ws >> scanner;
			if(scanner == 'J')
			{
				lexBuffer << scanner;
				*inputStream >> scanner;
				if(strchr("GL", scanner))
				{
					lexBuffer << scanner;
					*inputStream >>scanner;
					if(strchr("ET", scanner))
					{
						lexBuffer << scanner;
						jump = lexBuffer.str();
						return;
					}
				}

				else if(scanner == 'E' && inputStream->peek() == 'Q')
				{
					inputStream->get();
					lexBuffer << "EQ";
					jump = lexBuffer.str();
					return;
				}

				else if (scanner == 'N' && inputStream->peek() == 'E')
				{
					inputStream->get();
					lexBuffer << "NE";
					jump = lexBuffer.str();
					return;
				}

				else if (scanner == 'M' && inputStream->peek() == 'P')
				{
					inputStream->get();
					lexBuffer << "MP";
					jump = lexBuffer.str();
					return;
				}
			}
		}

		else 
		{
			// No jump component, return.
			inputStream->unget();
			return;
		}

		// Invalid command.
		throw std::logic_error("Bad C_COMMAND.");
	}
}

HackCommand Parser::commandType()
{
	return HackCommand();
}

bool Parser::isValidSymbolCharacter(char character)
{
	return (strchr("_.$:", character) || isalpha(character));
}
