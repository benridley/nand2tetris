#include <string>
#include <iostream>
#include <cstring>
#include <sstream>

enum class HackCommand
{
	A_COMMAND,
	L_COMMAND,
	C_COMMAND
};

class Parser
{
public:
	Parser();
	Parser(std::istream* inputStream);
	~Parser();

	// Returns true if there are more commands to be parsed in the input.
	bool hasMoreCommands();

	// Reads the next command from the input and makes it the current command.
	void advance();
	
	/*
	Returns the command type of the current command.
	This can be the following:
		A_COMMAND - @xxx where Xxx is a symbol, or decimal.
		C_COMMAND - Computation command. dest=comp;jump
		L_COMMAND - Label psuedo command (Xxx) where Xxx is a symbol.
	*/
	HackCommand commandType();

	// The following members hold the components of a command.
	std::string symbol;
	std::string dest;
	std::string jump;
	std::string comp;

private:
	// Holds the current input stream.
	std::istream* inputStream;
	bool isValidSymbolCharacter(char character);
	HackCommand currentCommandType;
};

