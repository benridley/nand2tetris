#include <iostream>
#include "Parser.h"
#include <fstream>
int main(int argc, char *argv[])
{
	if (!argv[1])
	{
		std::cout << "Please provide a file path." << std::endl;
		return 1;
	}
	try
	{
		Parser p(new std::fstream(argv[1]));

		while (p.hasMoreCommands())
		{
			p.advance();

			std::cout << "SYM: " << p.symbol << std::endl;
			std::cout << "DEST: " << p.dest << std::endl;
			std::cout << "COMP: " << p.comp << std::endl;
			std::cout << "JUMP: " << p.jump << std::endl;
		}

		std::cout << "Reached end of file." << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
		return 1;
	}
   
    return 0;
}