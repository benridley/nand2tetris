// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.

// Since multiplication is not supported by the ALU, multiplication is implemented in software
// by starting at zero and adding R1 repeatedly n times, where n = R1.

    @R2    // Some memory to store our sum.
    M
    =
    0     // Initialise sum to 0
    @R1     // Load the value in R1, which is our n value.
    D=M     // Store R1 in our data register
    @i      // Load our index
    M=D     // Set the index to the value of R1. 
(LOOP)
    @i
    D=M
    @END
    D;JEQ   // If i = 0, jump to end.
    @R0
    D=M
    @R2
    M=M+D   // Add R0 to the current sum value, and store the result in sum.
    @i
    M=M-1   // Decrement i.
    @LOOP
    0;JMP
(END)
    @END
    0;JMP
